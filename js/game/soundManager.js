var SoundManager = (function() {
    return function(soundVolume) {
        SoundManager.superclass.constructor.call(this);
        var audioMap = {};
        var audioVolume;
        var sounds = 0;
        var loadedSounds = 0;

        var myself = this;

        this.getAudioVolume = function() {
            return audioVolume * 100;
        };

        this.setAudioVolume = function(newVolume) {
            volume = newVolume / 100;

            if (volume >= 0 && volume <= 100) {
                for (i in audioMap) {
                    audioMap[i].volume(volume);
                }

                audioVolume = volume;
                return true;
            }

            return false;
        };

        this.getAudioMap = function() {
            return audioMap;
        };

        this.playSound = function(audioName) {
            if (audioMap[audioName]) {
                audioMap[audioName].play();
                return true;
            }

            return false;
        };

        function addSound(name, howlOptions) {
            sounds += 1;

            howlOptions.buffer = true;
            howlOptions.onload = function() {
                loadedSounds += 1;

                if (loadedSounds >= sounds) {
                    myself.fireEvent("soundsLoaded", {});
                }
            };

            audioMap[name + ""] = new Howl(howlOptions);
        }

        this.loadMenuSounds = function() {
            addSound(
                "menu1",
                {
                    urls: [
                        "./sounds/34628__Corsica_S__cd_flipper_02.ogg",
                        "./sounds/34628__Corsica_S__cd_flipper_02.wav"
                    ],
                    volume: audioVolume
                }
            );

            addSound(
                "menu2",
                {
                    urls: [
                        "./sounds/34632__Corsica_S__cd_flipper_06.ogg",
                        "./sounds/34632__Corsica_S__cd_flipper_06.wav"
                    ],
                    volume: audioVolume
                }
            );
        };

        this.loadGameSounds = function() {
            addSound(
                "death",
                {
                    urls: [
                        "./sounds/76969__Michel88__paino.ogg",
                        "./sounds/76969__Michel88__paino.wav"
                    ],
                    volume: audioVolume
                }
            );

            addSound(
                "attack1",
                {
                    urls: [
                        "./sounds/30248__Streety__sword7.ogg",
                        "./sounds/30248__Streety__sword7.wav"
                    ],
                    volume: audioVolume
                }
            );

            addSound(
                "attack2",
                {
                    urls: [
                        "./sounds/37596__hello_flowers__Sword.ogg",
                        "./sounds/37596__hello_flowers__Sword.wav"
                    ],
                    volume: audioVolume
                }
            );

            addSound(
                "boom",
                {
                    urls: [
                        "./sounds/33380__DJ_Chronos__Boom.mp3",
                        "./sounds/33380__DJ_Chronos__Boom.wav",
                        "./sounds/33380__DJ_Chronos__Boom.ogg"
                    ],
                    volume: audioVolume
                }
            );

            addSound(
                "boom2",
                {
                    urls: [
                        "./sounds/33245__ljudman__grenade.mp3",
                        "./sounds/33245__ljudman__grenade.wav",
                        "./sounds/33245__ljudman__grenade.ogg"
                    ],
                    volume: audioVolume
                }
            );
        };

        this.setAudioVolume(soundVolume);
    };
})();

extend(SoundManager, Events);
