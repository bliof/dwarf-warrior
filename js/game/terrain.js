var Terrain = (function() {
    return function(mainScene, documentName) {
        SimpleObject.call(this, mainScene, documentName);
        var heightmap;
        var terrainSize;

        /**
         * Returns all objects in the Collada
         */
        this.getObjects = function() {
            return objects;
        };

        /**
         * Adds an object to the Collada
         * @param {Object} object
         */
        this.getHeightMap = function() {
            return heightMap;
        };

        this.setHeightMap = function(heighmapUrl) {
            heightMap = new GLGE.HeightMap(heighmapUrl, 952, 1024, this.getMinX(), this.getMaxX(), this.getMaxY(), this.getMinY(), this.getMinZ(), this.getMaxZ());
        };
    };
})();

extend(Terrain, SimpleObject);
