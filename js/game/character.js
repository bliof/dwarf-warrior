var Character = function(mainScene, documentName, terrain) {
    SimpleObject.call(this, mainScene, documentName);
    var state;
    this.states = {
        ATTACK : 1,
        ATTACK_COOLDOWN : 2,
        DEATH : 3,
        MOVE : 4,
        STAND_STILL : 5
    };
    var myself = this;
    // Attack variables
    var damage;
    var attackDamage;
    var attackCooldownInterval = 30;
    var currentAttackCooldown = 0;

    var attackRange = 5.5;
    var hitspotRange = 5;

    // Health variables
    var health;
    var maxHealth = 100;
    var healthRegen;
    // tmp fix
    var hpRegenInterval = 0;
    var hpRegenStarted = false;

    // Target variables
    var target;
    var destinationToTarget;
    var orientationToTarget;

    // Forces variables
    var forces = [ 0, 0, 0 ];
    var velocity;

    this.getHitspotRange = function() {
        return hitspotRange;
    };
    this.getForces = function() {
        return forces;
    };
    this.getAttackCooldownInterval = function() {
        return attackCooldownInterval;
    };
    /**
     * Sets the cooldown of the attack
     *
     * @param {number}
     *            interval.
     */
    this.setAttackCooldownInterval = function(interval) {
        attackCooldownInterval = interval;
    };
    this.getCurrentAttackCooldown = function() {
        return currentAttackCooldown;
    };
    this.setCurrentAttackCooldown = function(interval) {
        currentAttackCooldown = interval;
    };
    this.isHpRegenStarted = function() {
        return hpRegenStarted;
    };
    /**
     * Returns the orientation to the target
     */
    this.getOrientationToTarget = function() {
        return orientationToTarget;
    };
    /**
     * Returns the destinationToTarget
     */
    this.getDestinationToTarget = function() {
        return destinationToTarget;
    };
    /**
     * Returns the attackRange of the unit
     */
    this.getAttackRange = function() {
        return attackRange;
    };
    this.addState = function(stateName) {
        var j = 1;
        for (i in this.states) {
            j++;
        }
        if (!this.states[stateName]) {
            this.states[stateName] = j;
        }
    };
    /**
     * Sets the state of the Character. States are defined outside the
     * Character class.
     *
     * @param {name}
     *            name
     */
    this.setState = function(name) {
        if (this.states[name]) {
            if (this.states[name] != state) {
                state = this.states[name];
                this.changeAnimation();
                this.fireEvent("stateChanged", {
                    newState : state
                });
            }
        }
    };
    this.changeAnimation = function() {
    };

    /**
     * Returns the state of the Character.
     */
    this.getState = function() {
        return state;
    };
    /**
     * Function is called when health is depleted. Removes the Character
     * from the field.
     */
    this.die = function() {
        this.setState("DEATH");
    };
    /**
     * Decreases the health of the Character.
     *
     * @param {number}
     *            damage - the damage.
     */
    this.getHit = function(damage) {
        health -= damage;
        if (health <= 0) {
            health = 0;
            this.die();
            return this.states.DEATH;
        }
        startHealthRegen();
        return this.states.STAND_STILL;
    };
    /**
     * Returns the speed of travel
     */
    this.getVelocity = function() {
        return velocity;
    };
    /**
     * Sets the velocity.
     *
     * @param {number}
     *            newVelocity.
     */
    this.setVelocity = function(newVelocity) {
        velocity = newVelocity;
    };
    /**
     * Sets the target, checks if it is just coordinates, or a Character.
     *
     * @param newTarget.
     */
    this.setTarget = function(newTarget) {
        if (state != this.states.DEATH) {
            if (newTarget instanceof Character) {
                if(newTarget.getState() == newTarget.states.DEATH) {
                    target = newTarget.getLoc();
                } else {
                    target = newTarget;
                }
                this.updateDestinationToTarget();
                return true;
            } else {
                if(newTarget instanceof Array) {
                    target = newTarget;
                    this.updateDestinationToTarget();
                    return true;
                }
            }
        }
        return false;
    };
    /**
     * Returns the current target
     */
    this.getTarget = function() {
        return target;
    };
    /**
     * Returns the current health
     */
    this.getHealth = function() {
        return health;
    };
    /**
     * Sets the health
     *
     * @param {number}
     *            hp.
     */
    this.setHealth = function(hp) {
        health = hp;
    };
    /**
     * Returns the maximum health of the Character.
     */
    this.getMaxHealth = function() {
        return maxHealth;
    };
    /**
     * Sets the maximum health a Character can have.
     *
     * @param {number}
     *            hp.
     */
    this.setMaxHealth = function(hp) {
        maxHealth = hp;
    };
    /**
     * Returns the speed of the regeneration of the health.
     */
    this.getHealthRegen = function() {
        return healthRegen;
    };
    /**
     * Sets the speed the health regenerates.
     *
     * @param {number}
     *            hpRegen.
     */
    this.setHealthRegen = function(hpRegen) {
        healthRegen = hpRegen;
    };
    /**
     * Sets the level of the Character.
     *
     * @param {number}
     *            lv.
     */
    this.setLevel = function(lv) {
        level = lv;
    };
    /**
     * Returns the level of the Character, used for calculating health,
     * attack , etc.
     */
    this.getLevel = function() {
        return level;
    };
    /**
     * Sets the damage.
     *
     * @param {number}
     *            dg.
     */
    this.setDamage = function(dg) {
        damage = dg;
    };
    /**
     * Returns the damage of the Character.
     */
    this.getDamage = function() {
        return damage;
    };
    /**
     * Initiates health regeneration
     */
    function startHealthRegen() {
        if (!hpRegenStarted) {
            hpRegenStarted = true;
        }
    }

    /**
     * Returns true if the coordinates passed are inside the body of the
     * Character.
     *
     * @param {Object}
     *            destination.
     */
    this.isTarget = function(destination) {
        if (destination[0] > (this.getX() - 2.5)
                && destination[0] < (this.getX() + 2.5)
                && destination[1] > (this.getY() - 2.5)
                && destination[1] < (this.getY() + 2.5))
            return true;
        else
            return false;
    };
    /**
     * Calculates the destination to the current target.
     */
    this.updateDestinationToTarget = function() {
        if (target) {
            var r;
            if (target instanceof Character) {
                r = GLGE.subVec3([ this.getX(), this.getY(), 0 ], [
                        target.getX(), target.getY(), 0 ]);
                orientationToTarget = GLGE.toUnitVec3(r);
                destinationToTarget = GLGE.lengthVec3(r);
                return true;
            }
            if (target instanceof Array) {
                r = GLGE.subVec3([ this.getX(), this.getY(), 0 ], [
                        target[0], target[1], 0 ]);
                orientationToTarget = GLGE.toUnitVec3(r);
                destinationToTarget = GLGE.lengthVec3(r);
                return true;
            }
        }
        return false;
    };
    /**
     * Checks if the Character can attack
     */
    this.cooldownAttack = function() {
        if (state != this.states.DEATH) {
            if (currentAttackCooldown > 0) {
                currentAttackCooldown--;
                return false;
            } else {
                return true;
            }
        }
    };
    /**
     * A straightforward implementation of the Lenard-Jones potential
     * function, which is (-A/r^n)+ (B/r^n)
     *
     * @param {vec3}
     *            targetLoc - the target's location
     * @param {Number}
     *            A - the strength of attraction
     * @param {Number}
     *            B - the strength of repulsion
     * @param {Number}
     *            n - attenuation of the attracting force
     * @param {Number}
     *            m - attenuation of the repulsing force
     * @param {Number}
     *            lengthRangeLimit - the top border of the destination
     *            between the two object; 0 for no limit
     */
    this.basePotentialFunction = function(targetLoc, A, B, n, m,
            lengthRangeLimit) {
        var myLoc = [ this.getX(), this.getY(), 0 ];
        var tarLoc = [ targetLoc[0], targetLoc[1], 0 ];
        var r = GLGE.subVec3(myLoc, tarLoc);
        var rLength = GLGE.lengthVec3(r);
        if (r[0] == 0 && r[1] == 0 && r[2] == 0) {
            return r;
        }
        if (lengthRangeLimit) {
            if (rLength > lengthRangeLimit) {
                return [ 0, 0, 0 ];
            }
        }
        var u = [ r[0] / rLength, r[1] / rLength, 0 ]; // GLGE.toUnitVec3(u);
        var U, d;
        d = rLength / this.getObjectLength();
        U = -A / Math.pow(d, n) + B / Math.pow(d, m);

        return GLGE.scaleVec3(u, U);
    };
    /**
     * Calculates the attraction to the target
     */
    this.attractionToTarget = function() {
        var A = 1000;
        var B = 1000;
        n = 0;
        m = 2;
        var U, d;
        d = destinationToTarget / this.getObjectLength();
        U = -A / Math.pow(d, n) + B / Math.pow(d, m);
        var myForces = GLGE.scaleVec3(orientationToTarget, U);
        return myForces;
    };
    /**
     * the target will be attracted to targetLoc
     *
     * @param {vec3}
     *            targetLoc
     */
    this.isAttractedTo = function(targetLoc) {
        return this.basePotentialFunction(targetLoc, 1000, 1000, 0, 2, 0);
    };
    /**
     * The target will be repulsed from this location
     *
     * @param {vec3}
     *            targetLoc
     */
    this.isRepulsedFrom = function(targetLoc) {
        return this.basePotentialFunction(targetLoc, 0, 1000, 1, 2.5, 50);
    };

    this.isRepulsedFromOstacle = function(obstacle) {
        return this.basePotentialFunction(obstacle.getLoc(), 0, obstacle.getRepulsionForce(), 1, 2.5, 50);
    };
    /**
     * Updates the attack
     */
    this.updateAttack = function() {
        var targetPos = target.getLoc();
        if(!target.states) {
            console.log("wtf");
        }

        if (target.states.DEATH == target.getHit(damage)) {
            this.setTarget(targetPos);
        }
        currentAttackCooldown = attackCooldownInterval;
    };
    /**
     * Manages the health regeneration process
     */
    this.updateHpRegen = function() {
        if (hpRegenStarted) {
            if (!hpRegenInterval) {
                health += healthRegen;
                hpRegenInterval = 30;
                if (health > maxHealth) {
                    health = maxHealth;
                    hpRegenStarted = false;
                }
            } else {
                hpRegenInterval--;
            }
        }
    };
    this.Lookat = function(value) {
        var objpos;
        var pos = this.getLoc();
        if (value.getLoc) {
            objpos = value.getLoc();
        } else {
            objpos = value;
        }
        var a, b, c;

        var a = objpos[0] - pos[0];
        var c = objpos[1] - pos[1];
        var b = Math.sqrt(a * a + c * c);
        if (b != 0) {
            var cosAngle = a / b;
            var sinAngle = c / b;
            this.getColladaObject().setRotMatrix(
                    GLGE.Mat4([ cosAngle, -sinAngle, 0, 0, sinAngle,
                            cosAngle, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 ]));
        }
    };
    /**
     * Updates the movement
     *
     * @param {object|array}
     *            obstacle- the obstacles that the unit should dodge
     */
    this.updateMovement = function(obstacle) {
        forces = this.attractionToTarget();

        for (var i in obstacle) {
            if (obstacle[i] instanceof SimpleObject) {
                if (this.getId() != obstacle[i].getId()) {
                    if (target instanceof Character) {
                        if (target.getId() != obstacle[i].getId())
                            forces = GLGE.addVec3(forces, this
                                    .isRepulsedFrom(obstacle[i].getLoc()));
                    } else
                        forces = GLGE.addVec3(forces, this
                                .isRepulsedFrom(obstacle[i].getLoc()));
                }
            } else if(obstacle[i] instanceof Obstacle) {
                forces = GLGE.addVec3(forces, this.isRepulsedFromOstacle(obstacle[i]));
            } else if (obstacle[i] instanceof Array) {
                forces = GLGE.addVec3(forces, this.isRepulsedFrom(obstacle[i]));
            }
        }
        forces = GLGE.scaleVec3(forces, 0.000025 * velocity);

        var newCoord = GLGE.addVec3(this.getLoc(), [ forces[0], forces[1],
                0 ]);
        this.Lookat(newCoord);

        newCoord[2] = terrain.getHeightMap().getHeightAt(newCoord[0],
                newCoord[1]);
        this.setLoc(newCoord);
    };
    /**
     * Updates the state of the Character.
     */
    this.updateState = function() {
        if (state != this.states.DEATH) {
            var canAttack = this.cooldownAttack();
            if (target) {
                this.updateDestinationToTarget();
                if (target.getHealth) {
                    if (destinationToTarget < attackRange) {
                        if (canAttack) {
                            this.setState("ATTACK");
                        } else {
                            this.setState("ATTACK_COOLDOWN");
                        }
                    } else {
                        this.setState("MOVE");
                    }
                } else {
                    if (destinationToTarget < hitspotRange) {
                        this.setState("STAND_STILL");
                    } else {
                        this.setState("MOVE");
                    }
                }
            }
        }
    };

    this.setHealth(100);
    this.setHealthRegen(1);

    this.setDamage(50);
    this.setLevel(1);

    this.setState("STAND_STILL");
    this.setVelocity(50);
    this.getWidth();
    startHealthRegen();
};

extend(Character, SimpleObject);
