var MainCharacter = function(mainScene, documentName, terrain, doc) {
    MainCharacter.superclass.constructor.call(this, mainScene, documentName, terrain);
    this.addState("ABILITY_USE");
    var myself = this;

    var maxMana = 80;
    var mana = 80;
    var manaRegen = 1;
    var manaRegenInterval = 0;
    var manaRegenStarted = false;

    var experience = 0;
    var experienceForLevelup;
    var activeAbility = 0;
    var level = 0;
    var abilities = [];
    var abilityUsed = false;

    this.getExperienceForLevelup = function() {
        return experienceForLevelup;
    };

    this.getExperience = function() {
        return experience;
    };

    this.addExperience = function(value) {
        experience += value;
        if (experience >= experienceForLevelup) {
            this.levelup();
        }
    };

    /**
     * Returns the level of the Character.
     */
    this.getLevel = function() {
        return level;
    };

    /**
     * level ups or sets the new level
     * @param {number} lv [optional]
     */
    this.levelup = function(lv) {
        if (lv) {
            level = lv;
        } else {
            level++;
        }
        experienceForLevelup = 500 * level + 100 * (level - 1);
        var tmpHpAdd = (level - 1) * 20;
        this.setMaxHealth(this.getMaxHealth() + tmpHpAdd);
        this.setHealth(this.getMaxHealth());
        this.setDamage(50 + 5 * (level - 1));
        var tmpManaAdd = (level - 1) * 15;
        this.setMaxMana(80 + tmpManaAdd);
        this.setMana(this.getMaxMana());
        this.fireEvent("levelup", {});
    };

    /**
     * Returns the ability
     * @param {Object} id - the id of the ability
     */
    this.getAbility = function(id) {
        return abilities[id];
    };

    /**
     * Returns the mana of the mainCharacter.
     */
    this.getMana = function() {
        return mana;
    };

    /**
     * Sets the maximum mana a mainCharacter can have.
     * @param {number} newMana
     */
    this.setMaxMana = function(newMana) {
        maxMana = newMana;
    };

    /**
     * Returns the maximum mana of the mainCharacter.
     */
    this.getMaxMana = function() {
        return maxMana;
    };

    /**
     * Sets the mana
     * @param {number} newMana
     */
    this.setMana = function(newMana) {
        mana = newMana;
    };

    /**
     * Returns the speed of mana regeneration of the MainCharacter.
     */
    this.getManaRegen = function() {
        return manaRegen;
    };

    /**
     * Sets the speed of regeneration
     * @param {number} newManaRegen
     */
    this.setManaRegen = function(newManaRegen) {
        manaRegen = newManaRegen;
    };

    /**
     * Starts the mana regeneration with an interval.
     */
    function startManaRegen() {
        if (!manaRegenStarted) {
            manaRegenStarted = true;
        }
    };

    this.updateManaRegen = function() {
        if (manaRegenStarted) {
            if (!manaRegenInterval) {
                mana += manaRegen;
                manaRegenInterval = 30;
                if (mana > maxMana) {
                    mana = maxMana;
                    manaRegenStarted = false;
                }
            } else {
                manaRegenInterval--;
            }
        }
    };

    /**
     * Called when the unit uses mana.
     * @param {number} negMana - the mana used.
     */
    this.useMana = function(negMana) {
        if (mana > negMana) {
            mana -= negMana;
            startManaRegen();
        }
    };

    /**
     * Sets the active ability of the MainCharacter.
     * @param {Object} id - the id of the ability.
     */
    this.setActiveAbility = function(id) {
        if (this.getState() != this.states.DEATH) {
            if (abilities[id].getLevel()) {
                if (activeAbility == id) {
                    activeAbility = 0;
                    return true;
                }
                mAbility = abilities[id];

                if (mAbility) {
                    if (!mAbility.getCurrCooldown()) {
                        if (this.getMana() > mAbility.getManaCost()) {
                            activeAbility = id;
                            if (id == MainCharacter.SECOND_ABILITY || id == MainCharacter.FOURTH_ABILITY) {
                                this.setState("ABILITY_USE");
                            }
                            return true;
                        } else {
                            //no mana
                        }
                    } else {
                        // cooldown
                    }
                }
            }
            return false;
        }
        return false;
    };

    /**
     * Returns the active ability of the MainCharacter.
     */
    this.getActiveAbility = function() {
        return activeAbility;
    };

    /**
     * Activates an ability.
     * @param {Object} target - the target of the ability.
     */
    this.activateAbility = function(target, enemies) {
        if (activeAbility) {
            mAbility = abilities[activeAbility];
            activeAbility = 0;
            if (!mAbility.getCurrCooldown() && (this.getMana() > mAbility.getManaCost())) {
                if (mAbility.activate(target, enemies)) {
                    myself.useMana(mAbility.getManaCost());
                }
            }
        }
    };

    /**
     * Updates the state of the MainCharacter.
     */
    this.updateState = function() {
        if (this.getState() != this.states.DEATH && this.getState() != this.states.ABILITY_USE) {
            var canAttack = this.cooldownAttack();
            if (this.getTarget()) {
                this.updateDestinationToTarget();
                if (this.getTarget().getHealth) {
                    if (this.getDestinationToTarget() < this.getAttackRange()) {
                        if (canAttack) {
                            this.setState("ATTACK");
                        } else {
                            this.setState("ATTACK_COOLDOWN");
                        }
                    } else {
                        this.setState("MOVE");
                    }
                } else {
                    if (this.getDestinationToTarget() < this.getHitspotRange()) {
                        this.setState("STAND_STILL");
                    } else {
                        this.setState("MOVE");
                    }
                }
            }
        }
    };

    /**
     * Checks the state of the character and sets the animation accordingly.
     */
    this.changeAnimation = function() {
        if (this.getState() == this.states.STAND_STILL) {
            this.getColladaObject().setStartFrame(90, 200, true);
            this.getColladaObject().setFrames(20);
            this.getColladaObject().setFrameRate(15);
        }
        if (this.getState() == this.states.MOVE) {
            this.getColladaObject().setStartFrame(16, 200, true);
            this.getColladaObject().setFrames(10);
            this.getColladaObject().setFrameRate(15);
        }
        if (this.getState() == this.states.ATTACK) {
            if (activeAbility == MainCharacter.FIRST_ABILITY) {
                this.getColladaObject().setStartFrame(128, 200, false);
                this.getColladaObject().setFrames(15);
                this.getColladaObject().setFrameRate(15);
            } else {
                this.getColladaObject().setStartFrame(112, 200, false);
                this.getColladaObject().setFrames(12);
                this.getColladaObject().setFrameRate(15);
            }
        }
        if (this.getState() == this.states.DEATH) {
            this.getColladaObject().setStartFrame(230, 200, false);
            this.getColladaObject().setFrames(21);
            this.getColladaObject().setFrameRate(10);
        }
        if (this.getState() == this.states.ABILITY_USE) {
            if (activeAbility == MainCharacter.SECOND_ABILITY) {
                this.getColladaObject().setStartFrame(144, 200, false);
                this.getColladaObject().setFrames(17);
                this.getColladaObject().setFrameRate(7);
                var secondAbAnim = setInterval( function() {
                    if (myself.getColladaObject().getFrameNumber() >= 161 || myself.getColladaObject().getFrameNumber() < 144) {
                        clearInterval(secondAbAnim);
                        abilityUsed = false;
                        myself.setState("STAND_STILL");
                    }
                }, 1);
            }
            if (activeAbility == MainCharacter.FOURTH_ABILITY) {
                this.getColladaObject().setStartFrame(56, 200, false);
                this.getColladaObject().setFrames(18);
                this.getColladaObject().setFrameRate(15);
                var fourthAbAnim = setInterval( function() {
                    if (myself.getColladaObject().getFrameNumber() >= 74 || myself.getColladaObject().getFrameNumber() < 56) {
                        clearInterval(fourthAbAnim);
                        abilityUsed = false;
                        myself.setState("STAND_STILL");
                    }
                }, 1);
            }
        }
    };

    /**
     * Calls all update functions
     * @param {object} obstacles
     */
    this.updateLogic = function(obstacles) {
        this.updateState();
        if (this.getState() != this.states.DEATH) {
            this.updateHpRegen();
            this.updateManaRegen();
            for (i = 1; i <= 4; i++) {
                abilities[i].updateCooldown();
            }
            switch (this.getState()) {
            case this.states.ABILITY_USE:
                if (!abilityUsed) {
                    var enemies = [];
                    for (i in obstacles) {
                        if (obstacles[i] instanceof NPCCharacter) {
                            if (obstacles[i].getState() != obstacles[i].states.DEATH) {
                                enemies.push(obstacles[i]);
                            }
                        }
                    }
                    abilityUsed = true;
                    this.activateAbility(this.getTarget(), enemies);
                }
                break;
            case this.states.MOVE:
                this.updateMovement(obstacles);
                break;
            case this.states.ATTACK:
                if (this.getTarget() instanceof Array) {
                    this.Lookat(this.getTarget());
                    return;
                } else {
                    this.Lookat(this.getTarget().getLoc());
                }

                this.updateAttack();
                if (activeAbility == MainCharacter.FIRST_ABILITY) {
                    this.activateAbility(this.getTarget(), enemies);
                }
                break;
            case this.states.ATTACK_COOLDOWN:
                if (this.getTarget()) {
                    this.Lookat(this.getTarget());
                }
                break;
            }
        }
    };

    function createLightning(width, size, loc) {
        var positions = [];
        var length = Math.abs(size);
        for (var i = 0; i < length; i++) {
            positions.push(0);
            positions.push(0);
            positions.push(i);
            positions.push(0);
            positions.push(0);
            positions.push((i + 1));
        }
        var line = (new GLGE.Object).setDrawType(GLGE.DRAW_LINES).setLineWidth(width);
        line.setMesh((new GLGE.Mesh).setPositions(positions));
        var colour = doc.getElement("mayaBlue");
        line.setMaterial(colour);
        mainScene.addChild(line);
        line.setLoc(loc[0], loc[1], loc[2] + 20);

        var injection = [];
        injection.push("uniform float time;\n");
        injection.push("vec4 GLGE_Position(vec4 pos){\n");
        injection.push("pos.z=pos.z+(fract(sin(dot(pos.xy+vec2(time), vec2(12.9898,78.233))) * 43758.5453)-0.5)*0.5;\n");
        injection.push("pos.y=pos.y+(fract(sin(dot(pos.zx+vec2(time), vec2(12.9898,78.233))) * 43758.5453)-0.5)*0.5;\n");
        injection.push("pos.x=pos.x+(fract(sin(dot(pos.yz+vec2(time), vec2(12.9898,78.233))) * 43758.5453)-0.5)*0.5;\n");
        injection.push("return pos;\n");
        injection.push("}\n");

        line.setVertexShaderInjection(injection.join(""));

        var lightningCooldown = 20;
        var lightningIntervalId;
        function decreaseLightningCooldown() {
            if (lightningCooldown > 0) {
                lightningCooldown -= 0.5;
            } else {
                clearInterval(lightningIntervalId);
                mainScene.removeChild(line);
            }
        }

        lightningIntervalId = setInterval( function() {
            decreaseLightningCooldown();
            loc[2]--;
            line.setLocZ(loc[2]);
            line.setUniform("1f", "time", lightningCooldown / 10000);
        }, 1);
    }

    /**
     * Builds the first ability - Lightning Fury
     */
    function buildFirstAbility() {
        var firstAbility = new Ability("Lightning Fury", 1, 45, 100, 20);
        firstAbility.activate = function(target, enemies) {
            var targetPos;
            if (target instanceof Character) {
                var targetPos = target.getLoc();
                if (target.getHit(this.getDmg()) == target.states.DEATH) {
                    myself.setTarget(targetPos);
                }
            } else {
                targetPos = target;
            }
            createLightning(3, 30, [targetPos[0], targetPos[1], targetPos[2]]);
            this.startCooldown();
            return true;
        };
        return firstAbility;
    }

    /**
     * Builds the second ability - Lightning Nova
     */
    function buildSecondAbility() {
        var secondAbility = new Ability("Lightning Nova", 2, 50, 150, 25);
        secondAbility.range = 20;
        secondAbility.activate = function(target, enemies) {
            for (i in enemies) {
                var dest = GLGE.lengthVec3(GLGE.subVec3(myself.getLoc(), enemies[i].getLoc()));
                if (dest < this.range) {
                    enemies[i].setState("STUNNED");
                    enemies[i].getHit(this.getDmg());
                }
            }
            this.startCooldown();
            return true;
        };
        return secondAbility;
    }

    /**
     * Builds the third ability - Teleportation
     */
    function buildThirdAbility() {
        var thirdAbility = new Ability("Teleportation", 3, 0, 150, 10);
        var teleportRange = 50;
        thirdAbility.activate = function(target, enemies) {
            var newX;
            var newY;
            var forces;
            if (target instanceof NPCCharacter) {
                newX = target.getX() - target.getObjectLength();
                newY = target.getY() - target.getObjectLength();
                myself.setTarget(target);
            } else {
                newX = target[0];
                newY = target[1];
                myself.setTarget(target);
            }
            if (myself.getDestinationToTarget() > teleportRange) {
                myself.setX(myself.getX() + teleportRange * (-myself.getOrientationToTarget()[0]));
                myself.setY(myself.getY() + teleportRange * (-myself.getOrientationToTarget()[1]));
            } else {
                myself.setX(newX);
                myself.setY(newY);
            }
            myself.setZ(terrain.getHeightMap().getHeightAt(myself.getX(), myself.getY()));
            this.startCooldown();
            return true;
        };
        return thirdAbility;
    }

    /**
     * Builds the fourth ability = Thunderstruck
     */
    function buildFourthAbility() {
        var fourthAbility = new Ability("Thunderstruck", 4, 200, 700, 45);
        fourthAbility.activate = function(target, enemies) {
            for (i in enemies) {
                createLightning(3, 30, enemies[i].getLoc());
                enemies[i].getHit(this.getDmg());
            }
            this.startCooldown();
            return true;
        };
        return fourthAbility;
    }

    /**
     * Adds all of the abilities to the MainCharacter
     */
    function setAbilities() {
        abilities.push(false);
        abilities.push(buildFirstAbility());
        abilities.push(buildSecondAbility());
        abilities.push(buildThirdAbility());
        abilities.push(buildFourthAbility());
    }

    setAbilities();
    this.levelup();
};

MainCharacter.FIRST_ABILITY = 1;
MainCharacter.SECOND_ABILITY = 2;
MainCharacter.THIRD_ABILITY = 3;
MainCharacter.FOURTH_ABILITY = 4;

extend(MainCharacter, Character);
