/**
* @class A events class
**/
Events = function() {}

/**
* Fires an event
* @param {string} event The name of the event to fire
* @param {object} data the events data
**/
Events.prototype.fireEvent = function(event, data) {
    if (this.events && this.events[event]) {
        var events = this.events[event];

        for (var i = 0; i < events.length; i++) {
            events[i].call(this, data);
        }
    }
}

/**
* Adds an event listener
* @param {string} event The name of the event to listen for
* @param {function} func the event callback
**/
Events.prototype.addEventListener = function(event, func) {
    if (!this.events) this.events = {};

    if (!this.events[event]) this.events[event] = [];

    this.events[event].push(func);
}

/**
* Removes an event listener
* @param {function} func the event callback to remove
**/
Events.prototype.removeEventListener = function(event, func) {
    if (!this.events[event]) return;

    var index = this.events[event].indexOf(func);

    if (index != -1) this.events[event].splice(index, 1);
}
