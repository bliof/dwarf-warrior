var Obstacle = function(myX, myY, myZ, myRepulsionForce) {
    var x, y, z;
    var repulsionForce;

    this.getRepulsionForce = function() {
        return repulsionForce;
    };
    this.setRepulsionForce = function(mRepulsionForce) {
        repulsionForce = mRepulsionForce;
    };

    this.getLocX = function() {
        return x;
    };
    this.setLocX = function(mX) {
        x = mX;
    };

    this.getLocY = function() {
        return y;
    };
    this.setLocY = function(mY) {
        y = mY;
    };

    this.getLocZ = function() {
        return z;
    };
    this.setLocZ = function(mZ) {
        this.z = mZ;
    };

    this.getLoc = function() {
        return [x, y, z];
    };

    this.setLocX(myX);
    this.setLocY(myY);
    this.setLocZ(myZ);
    this.setRepulsionForce(myRepulsionForce);
};
