function Ability(mName, mType, startingDmg, startingCooldown, startingManaCost) {
    var name = mName;
    var type = mType;
    var dmg = startingDmg;
    var cooldown = startingCooldown;
    var currCooldown = 0;
    var manaCost = startingManaCost;

    var level = 0;
    var rdyForLevelup;

    this.isRdyForLevelup = function() {
        return rdyForLevelup;
    };

    this.setRdyForLevelup = function(bool) {
        rdyForLevelup = bool;
    };

    function decreaseCooldown() {
        cooldown -= cooldown * 0.2;
    }

    function increaseDmg() {
        dmg += dmg * 0.2;
    }

    function increaseManaCost() {
        manaCost += manaCost * 0.2;
    }

    this.getName = function() {
        return name;
    };

    this.getType = function() {
        return type;
    };

    this.levelup = function() {
        level++;
        decreaseCooldown();
        increaseManaCost();
        increaseDmg();
    };

    this.getDmg = function() {
        return dmg;
    };

    this.getCurrCooldown = function() {
        return currCooldown;
    };

    this.getManaCost = function() {
        return manaCost;
    };

    this.getLevel = function() {
        return level;
    };

    this.startCooldown = function() {
        currCooldown = cooldown;
    };

    this.updateCooldown = function() {
        if (currCooldown > 0) {
            currCooldown--;
            if(currCooldown < 0) {
                currCooldown=0;
            }
        }
    };

    if (type != 4) {
        this.setRdyForLevelup(true);
    } else {
        this.setRdyForLevelup(false);
    }
}
