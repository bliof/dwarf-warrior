var SimpleObject = (function() {
    var objectCounter = 0;
    function incObjectCounter() {
        objectCounter++;
    }

    return function(mainScene, documentName) {
        SimpleObject.superclass.constructor.call(this);
        var id = objectCounter;
        incObjectCounter();

        var objectLength;
        var objectHeight;
        var objectWidth;

        var colladaObject = new GLGE.Collada();
        colladaObject.setDocument(documentName);
        colladaObject.addEventListener("loaded", function() {
            mainScene.addChild(colladaObject);
        });
        this.getObjectHeight = function() {
            return objectHeight;
        };
        this.setObjectHeight = function(newHeight) {
            objectHeight = newHeight;
        };
        /**
         * Sets the lenght of the object
         *
         * @param {Object}
         *            newObjectLenght
         */
        this.setObjectLength = function(newObjectLength) {
            objectLength = newObjectLength;
        };
        /**
         * Returns the object lenght
         *
         */
        this.getObjectLength = function() {
            return objectLength;
        };
        /**
         * Returns maxX - minX
         */
        this.getWidth = function() {
            var boundings = colladaObject.getBoundingVolume();
            return (boundings.limits[1] - boundings.limits[0]);
        };
        /**
         * Returns maxY - minY
         */
        this.getHeight = function() {
            var boundings = colladaObject.getBoundingVolume();
            return (boundings.limits[3] - boundings.limits[2]);
        };
        /**
         * Returns minX
         */
        this.getMinX = function() {
            var boundings = colladaObject.getBoundingVolume();
            return boundings.limits[0];
        };
        /**
         * Returns maxX
         */
        this.getMaxX = function() {
            var boundings = colladaObject.getBoundingVolume();
            return boundings.limits[1];
        };
        /**
         * Returns minY
         *
         */
        this.getMinY = function() {
            var boundings = colladaObject.getBoundingVolume();
            return boundings.limits[2];
        };
        /**
         * Returns maxY
         *
         */
        this.getMaxY = function() {
            var boundings = colladaObject.getBoundingVolume();
            return boundings.limits[3];
        };
        /**
         * Returns minZ
         *
         */
        this.getMinZ = function() {
            var boundings = colladaObject.getBoundingVolume();
            return boundings.limits[4];
        };
        /**
         * Returns maxZ
         *
         */
        this.getMaxZ = function() {
            var boundings = colladaObject.getBoundingVolume();
            return boundings.limits[5];
        };
        /**
         * Gets the X position of the object
         *
         */
        this.getX = function() {
            return colladaObject.getLocX();
        };
        /**
         * Sets the X position of the object
         *
         * @param {number}
         *            location
         */
        this.setX = function(location) {
            colladaObject.setLocX(location);
        };
        /**
         * Sets the scale of the collada object
         *
         * @param {number}
         *            scale
         */
        this.setScale = function(scale) {
            colladaObject.setScale(scale);
        };
        /**
         * Returns the scale of the collada object.
         *
         */
        this.getScale = function() {
            colladaObject.getScale();
        };
        /**
         * Sets the X scale of the object
         *
         * @param {number}
         *            scale
         */
        this.setScaleX = function(scale) {
            colladaObject.setScaleX(scale);
        };
        /**
         * Returns X scale of the collada object.
         *
         */
        this.getScaleX = function() {
            colladaObject.getScaleX();
        };
        /**
         * Sets the Y scale of the object
         *
         * @param {number}
         *            scale
         */
        this.setScaleY = function(scale) {
            colladaObject.setScaleY(scale);
        };
        /**
         * Returns Y scale of the collada object.
         *
         */
        this.getScaleY = function() {
            colladaObject.getScaleY();
        };
        /**
         * Sets the Z scale of the object
         *
         * @param {number}
         *            scale
         */
        this.setScaleZ = function(scale) {
            colladaObject.setScaleZ(scale);
        };
        /**
         * Returns Z scale of the collada object.
         *
         */
        this.getScaleZ = function() {
            colladaObject.getScaleZ();
        };
        /**
         * Returns Y position of the collada object.
         *
         */
        this.getY = function() {
            return colladaObject.getLocY();
        };
        /**
         * Sets the Y location of the collada object.
         *
         * @param {number}
         *            location
         */
        this.setY = function(location) {
            colladaObject.setLocY(location);
        };
        /**
         * Gets the Z position of the object.
         *
         */
        this.getZ = function() {
            return colladaObject.getLocZ();
        };
        /**
         * Sets the X location of the collada object.
         *
         * @param {number}
         *            location
         */
        this.setZ = function(location) {
            colladaObject.setLocZ(location);
        };
        /**
         * Returns the position [X,Y,Z] of the colladaObject
         */
        this.getLoc = function() {
            return [ this.getX(), this.getY(), this.getZ() ];
        };
        /**
         * Sets the location of the collada object.
         *
         * @param {locVec3}
         *            locVec3
         */
        this.setLoc = function(locVec3) {
            this.setX(locVec3[0]);
            this.setY(locVec3[1]);
            this.setZ(locVec3[2]);
        };
        /**
         * Returns the id of the simpleObject.
         */
        this.getId = function() {
            return id;
        };
        /**
         * Returns the collada object
         */
        this.getColladaObject = function() {
            return colladaObject;
        };
        this.setObjectLength(4);
        this.setObjectHeight(3);
    };
})();

extend(SimpleObject, Events);
