function LevelManager(menuSoundVolume){
    LevelManager.superclass.constructor.call(this);
    var myself = this;
    var gameRenderer = new GLGE.Renderer(document.getElementById('canvas'));
    var inputHandler;
    var now;
    var lasttime = 0;
    var gameScene = new GLGE.Scene();
    var gameState = new GameState();
    var soundManager = new SoundManager(menuSoundVolume);
    var gameLevel = 1;

    var levelBuilder = new LevelBuilder();
    var level;

    this.getLevel = function(){
        return level;
    };
    this.getSoundManager = function(){
        return soundManager;
    };
    /**
     * Returns the gameScene
     */
    this.getGameScene = function(){
        return gameScene;
    };
    this.getNow = function(){
        return now;
    };
    this.getLastTime = function(){
        return lasttime;
    };
    /**
     * Returns the state of the game
     */
    this.getGameState = function(){
        return gameState;
    };
    /**
     * Returns the level of the game.
     */
    this.getGameLevel = function(){
        return gameLevel;
    };
    /**
     * Checks if a collision has accoured and handles it.
     */
    function checkForCollisions(){
        for (var i in nonMovingObjects) {
            if (level.myChar.getCollisionDetector().DetectAABBCollisionPosition(nonMovingObjects[i])) {
            }
        }
        return true;
    };
    /**
     * Checks if all of the enemies are dead and if so, removes them from the field,
     * and returns true.
     */
    function checkIfAllAreDead(){
        for (var i in level.enemies) {
            if (level.enemies[i].getState() != level.enemies[i].states.DEATH)
                return false;
        }
        level.myChar.setTarget(level.myChar.getLoc());
        return true;
    };

    /**
     * Processes the mouse input. Detects if an object is clicked and if so,
     * sets is as a target
     * @param {Array} target - the coordinates clicked (in 3D space)
     * @param {Object} obj - the object that is clicked
     */
    this.processMouseInput = function(target, obj){
        for (var i in level.enemies) {
            var objectsInCollada = level.enemies[i].getColladaObject().getObjects();
            for (var j in objectsInCollada) {
                if (objectsInCollada[j] == obj) {
                    if (level.myChar.getActiveAbility() == MainCharacter.THIRD_ABILITY) {
                        level.myChar.activateAbility(level.enemies[i]);
                        return true;
                    }
                    level.myChar.setTarget(level.enemies[i]);
                    return true;
                }
            }
        }

        if (target instanceof Array) {
            if (level.myChar.getActiveAbility() == MainCharacter.THIRD_ABILITY) {
                level.myChar.activateAbility(target);
                return true;
            }
            level.myChar.setTarget(target);
            return true;
        }
        return false;
    };

    function updateGameLogic(){
        var aliveEnemies = [];
        for (var i in level.enemies) {
            if (level.enemies[i].getState() != level.enemies[i].states.DEATH) {
                aliveEnemies.push(level.enemies[i]);
            }
        }
        var allObjects = aliveEnemies.concat(level.nonMovingObjects);

        level.myChar.updateLogic(allObjects, level.terrain);
        for (var j in aliveEnemies) {
            aliveEnemies[j].updateLogic(allObjects, level.terrain);
        }

    };

    function updateAbilityCooldownTimers() {
        var abilityCooldown = Math.round(level.myChar.getAbility(MainCharacter.FIRST_ABILITY).getCurrCooldown()/30)+1;
        if(level.myChar.getAbility(MainCharacter.FIRST_ABILITY).getCurrCooldown()==0) {
            abilityCooldown="";
        }
        document.getElementById("firstAb").innerHTML=abilityCooldown;
        abilityCooldown = Math.round(level.myChar.getAbility(MainCharacter.SECOND_ABILITY).getCurrCooldown()/30)+1;
        if(level.myChar.getAbility(MainCharacter.SECOND_ABILITY).getCurrCooldown()==0) {
            abilityCooldown="";
        }
        document.getElementById("secondAb").innerHTML= abilityCooldown;
        abilityCooldown = Math.round(level.myChar.getAbility(MainCharacter.THIRD_ABILITY).getCurrCooldown()/30)+1;
        if(level.myChar.getAbility(MainCharacter.THIRD_ABILITY).getCurrCooldown()==0) {
            abilityCooldown="";
        }
        document.getElementById("thirdAb").innerHTML=abilityCooldown;
        abilityCooldown = Math.round(level.myChar.getAbility(MainCharacter.FOURTH_ABILITY).getCurrCooldown()/30)+1;
        if(level.myChar.getAbility(MainCharacter.FOURTH_ABILITY).getCurrCooldown()==0) {
            abilityCooldown="";
        }
        document.getElementById("fourthAb").innerHTML= abilityCooldown;
    }

    var startRendering = function(){
        var frameratebuffer = 60;
        start = parseInt(new Date().getTime());
        function render(){
            now = parseInt(new Date().getTime());
            if (gameState.getState() == gameState.getStateValues().SIMULATION) {
                updateGameLogic();

                if (level.myChar != null) {
                    $("#healthBar").progressbar("option", "value", (level.myChar.getHealth() / level.myChar.getMaxHealth()) * 100);
                    $("#manaBar").progressbar("option", "value", (level.myChar.getMana() / level.myChar.getMaxMana()) * 100);
                    document.getElementById("heroLevel").innerHTML = "Hero level: " + level.myChar.getLevel();
                    document.getElementById("heroDmg").innerHTML = "Damage: " + level.myChar.getDamage();
                    document.getElementById("gameLevel").innerHTML = "Level:" + gameLevel;
                    document.getElementById("score").innerHTML = "Score:" + level.myChar.getExperience();
                    updateAbilityCooldownTimers();
                }
                inputHandler.checkKeys();
                gameScene.getCamera().setLocZ(level.myChar.getZ() + 70);
                gameScene.getCamera().setLocY(level.myChar.getY() + 65);
                gameScene.getCamera().setLocX(level.myChar.getX());
                gameRenderer.render();
                frameratebuffer = Math.round(((frameratebuffer * 9) + 1000 / (now - lasttime)) / 10);
                document.getElementById("debug").innerHTML = "Frame Rate:" + frameratebuffer;
                lasttime = now;
            }
        }
        setInterval(render, 29);
    };

    /**
     * Builds the scene.
     * @param {Object} levelXML- the XML of a level
     */
    this.buildGameScene = function(levelXML){
        var doc = new GLGE.Document();
        doc.onLoad = function(){
            gameScene = doc.getElement("mainscene");
            gameScene.getCamera().setAspect(document.getElementById("canvas").offsetWidth / document.getElementById("canvas").offsetHeight);

            level = levelBuilder.createLevel(gameScene, gameLevel, doc);
        };
        doc.load(levelXML);
    };

    function setAbilityLevelupButtonVisibility(){
        if (level.myChar.getAbility(1).isRdyForLevelup()) {
            if (!$("#firstAbilityLevelUpIcon").is(":visible")) {
                $("#firstAbilityLevelUpIcon").show();
            }
        }
        else {
            if ($("#firstAbilityLevelUpIcon").is(":visible")) {
                $("#firstAbilityLevelUpIcon").hide();
            }
        }

        if (level.myChar.getAbility(2).isRdyForLevelup()) {
            if (!$("#secondAbilityLevelUpIcon").is(":visible")) {
                $("#secondAbilityLevelUpIcon").show();
            }
        }
        else {
            if ($("#secondAbilityLevelUpIcon").is(":visible")) {
                $("#secondAbilityLevelUpIcon").hide();
            }
        }

        if (level.myChar.getAbility(3).isRdyForLevelup()) {
            if (!$("#thirdAbilityLevelUpIcon").is(":visible")) {
                $("#thirdAbilityLevelUpIcon").show();
            }
        }
        else {
            if ($("#thirdAbilityLevelUpIcon").is(":visible")) {
                $("#thirdAbilityLevelUpIcon").hide();
            }
        }

        if (level.myChar.getAbility(4).isRdyForLevelup()) {
            if (!$("#fourthAbilityLevelUpIcon").is(":visible")) {
                $("#fourthAbilityLevelUpIcon").show();
            }
        }
        else {
            if ($("#fourthAbilityLevelUpIcon").is(":visible")) {
                $("#fourthAbilityLevelUpIcon").hide();
            }
        }
    }

    function levelupAbility(e){
        if (level.myChar.getState() != level.myChar.states.DEATH) {
            var totalTimesLeveledAbilities = 0;
            var totalTimesLeveledOtherAbilities = 0;

            for (var i = 1; i <= 4; i++) {
                totalTimesLeveledAbilities += level.myChar.getAbility(i).getLevel();
                if (e.abilityId != i) {
                    totalTimesLeveledOtherAbilities += level.myChar.getAbility(i).getLevel();
                }
            }

            if (level.myChar.getAbility(e.abilityId).isRdyForLevelup() && (level.myChar.getLevel() > totalTimesLeveledAbilities)) {
                level.myChar.getAbility(e.abilityId).levelup();
                totalTimesLeveledAbilities++;

                if (level.myChar.getAbility(e.abilityId).getLevel() > totalTimesLeveledOtherAbilities) {
                    level.myChar.getAbility(e.abilityId).setRdyForLevelup(false);
                    setAbilityLevelupButtonVisibility();
                }
                if (e.abilityId == MainCharacter.FOURTH_ABILITY) {
                    if (level.myChar.getLevel() < 5 * (level.myChar.getAbility(e.abilityId).getLevel() + 1)) {
                        level.myChar.getAbility(e.abilityId).setRdyForLevelup(false);
                        setAbilityLevelupButtonVisibility();
                    }
                }

                for (var i = 1; i <= 4; i++) {
                    if (e.abilityId != i) {
                        if (i != 4) {
                            level.myChar.getAbility(i).setRdyForLevelup(true);
                        }
                        else {
                            if (level.myChar.getLevel() >= 5 * (level.myChar.getAbility(i).getLevel() + 1)) {
                                level.myChar.getAbility(i).setRdyForLevelup(true);
                            }
                        }
                    }
                }

                if (level.myChar.getLevel() - totalTimesLeveledAbilities == 0) {
                    $(".plus").hide();
                }
            }
        }
    }

    function initializeInputHandler() {
        inputHandler = new InputHandler(myself);
        inputHandler.addEventListener("pausedPressed", function(){
            if (this.levelManager.getGameState().getState() == this.levelManager.getGameState().getStateValues().PAUSEDSIMULATION) {
                this.levelManager.getGameState().setState("SIMULATION");
                document.getElementById("menu").style.visibility = "hidden";
                document.getElementById("game_container").style.visibility = "visible";
            }
            else {
                if (this.levelManager.getGameState().getState() == this.levelManager.getGameState().getStateValues().SIMULATION) {
                    this.levelManager.getGameState().setState("PAUSEDSIMULATION");
                    document.getElementById("menu").style.visibility = "visible";
                    document.getElementById("game_container").style.visibility = "hidden";
                }
            }
        });
    };

    var soundSwapper = true;
    function addEventsListenersToOpponents(){
        for (i in level.enemies) {
            level.enemies[i].addEventListener("stateChanged", function(){
                if (this.getState() == this.states.DEATH) {
                    soundManager.playSound("death");

                    level.myChar.addExperience(this.getPoints());
                    if (checkIfAllAreDead() == true) {
                        gameLevel++;
                        levelBuilder.resetEnemiesStats(gameScene, gameLevel, level);
                    }
                }
                if (this.getState() == this.states.ATTACK) {
                    if (soundSwapper == true)
                        soundManager.playSound("attack1");
                    else
                        soundManager.playSound("attack2");
                    soundSwapper = !soundSwapper;
                }
            });
        }
    }

    function addEventsListenersToMainCharacter() {
        level.myChar.addEventListener("stateChanged", function(){
            if (this.getState() == this.states.DEATH) {
                soundManager.playSound("death");
                myself.getGameState().setState("END");
                myself.fireEvent("END", {score:level.myChar.getExperience()});
            }

            if (this.getState() == this.states.DEATH) {
                soundManager.playSound("death");
            }
            if (this.getState() == this.states.ATTACK) {
                if (soundSwapper == true)
                    soundManager.playSound("attack1");
                else
                    soundManager.playSound("attack2");
                soundSwapper = !soundSwapper;
                if (this.getActiveAbility() == MainCharacter.FIRST_ABILITY) {
                    soundManager.playSound("boom");
                }
            }
            if (this.getState() == this.states.ABILITY_USE) {
                if (this.getActiveAbility() == MainCharacter.FOURTH_ABILITY) {
                    soundManager.playSound("boom2");
                }
            }
        });

        level.myChar.addEventListener("levelup", setAbilityLevelupButtonVisibility);
        level.myChar.addEventListener("abilityLeveled", levelupAbility);
    }

    function setMainCharacterInitialState() {
         level.myChar.Lookat([0, -20, 0]);
         level.myChar.getColladaObject().setLocZ(level.terrain.getHeightMap().getHeightAt(level.myChar.getX(), level.myChar.getY()));
         level.myChar.changeAnimation();
    }

    /**
     * Initiates the game
     */
    this.loadGame = function(){
        gameState.setState("LOADING");
        $("#loading").html("Loading meshes...");
        myself.buildGameScene("level.xml");

        soundManager.addEventListener("soundsLoaded", function(){
            initializeInputHandler();

            gameRenderer.setScene(gameScene);

            document.getElementById("loading").style.visibility = 'hidden';
            document.getElementById("gameTitle").style.visibility = 'hidden';
            document.getElementById("game_container").style.visibility = 'visible';
            $(".plus").hide();
            setAbilityLevelupButtonVisibility();

            addEventsListenersToOpponents();
            addEventsListenersToMainCharacter();

            gameState.setState("SIMULATION");

            setMainCharacterInitialState();
            startRendering();
        });

        levelBuilder.addEventListener("sceneLoaded", function(){
                soundManager.loadGameSounds();
        });
    };
};

extend(LevelManager, Events);
