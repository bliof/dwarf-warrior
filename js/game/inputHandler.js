var InputHandler = function(mLevelManager) {
    InputHandler.superclass.constructor.call(this);
    this.levelManager = mLevelManager;
    var myself = this;
    var mouse = new GLGE.MouseInput(document.getElementById('canvas'));
    var keys = new GLGE.KeyInput();
    var mouseOverCanvas;

    var isPauseKeyUp = true;
    var isQKeyUp = true;
    var isWKeyUp = true;
    var isEKeyUp = true;
    var isRKeyUp = true;
    var isSHIFTKeyUp = true;

    /**
     * Checks for keyboard input.
     */
    this.checkKeys = function() {
        if (this.levelManager.getLevel().myChar.getState() != this.levelManager.getLevel().myChar.states.DEATH) {
            if (keys.isKeyPressed(GLGE.KI_PAUSE_BREAK) || keys.isKeyPressed(GLGE.KI_P)) {
                if (isPauseKeyUp) {
                    this.fireEvent("pausedPressed", {});
                    isPauseKeyUp = false;
                }
            } else {
                isPauseKeyUp = true;
            }
            if (this.levelManager.getGameState().getState() == this.levelManager.getGameState().getStateValues().SIMULATION) {

                if (keys.isKeyPressed(GLGE.KI_SHIFT)) {
                    if (isSHIFTKeyUp) {
                        isSHIFTKeyUp = false;
                    }
                } else {
                    isSHIFTKeyUp = true;
                }

                if (keys.isKeyPressed(GLGE.KI_Q)) {
                    if (isQKeyUp) {
                        if (isSHIFTKeyUp) {
                            this.levelManager.getLevel().myChar.setActiveAbility(MainCharacter.FIRST_ABILITY);
                        } else {
                            mLevelManager.getLevel().myChar.fireEvent("abilityLeveled", {
                                abilityId: MainCharacter.FIRST_ABILITY
                            });
                        }
                        isQKeyUp = false;
                    }
                } else {
                    isQKeyUp = true;
                }

                if (keys.isKeyPressed(GLGE.KI_W)) {
                    if (isWKeyUp) {
                        if (isSHIFTKeyUp) {
                            this.levelManager.getLevel().myChar.setActiveAbility(MainCharacter.SECOND_ABILITY);
                        } else {
                            mLevelManager.getLevel().myChar.fireEvent("abilityLeveled", {
                                abilityId: MainCharacter.SECOND_ABILITY
                            });
                        }
                        isWKeyUp = false;
                    }
                } else {
                    isWKeyUp = true;
                }

                if (keys.isKeyPressed(GLGE.KI_E)) {
                    if (isEKeyUp) {
                        if (isSHIFTKeyUp) {
                            this.levelManager.getLevel().myChar.setActiveAbility(MainCharacter.THIRD_ABILITY);
                        } else {
                            mLevelManager.getLevel().myChar.fireEvent("abilityLeveled", {
                                abilityId: MainCharacter.THIRD_ABILITY
                            });
                        }
                        isEKeyUp = false;
                    }
                } else {
                    isEKeyUp = true;
                }

                if (keys.isKeyPressed(GLGE.KI_R)) {
                    if (isRKeyUp) {
                        if (isSHIFTKeyUp) {
                            this.levelManager.getLevel().myChar.setActiveAbility(MainCharacter.FOURTH_ABILITY);
                        } else {
                            mLevelManager.getLevel().myChar.fireEvent("abilityLeveled", {
                                abilityId: MainCharacter.FOURTH_ABILITY
                            });
                        }
                        isRKeyUp = false;
                    }
                } else {
                    isRKeyUp = true;
                }
            }
        }
    };

    $("#firstAbilityLevelUpIcon").click( function() {
        mLevelManager.getLevel().myChar.fireEvent("abilityLeveled", {
            abilityId: MainCharacter.FIRST_ABILITY
        });
    });
    $("#secondAbilityLevelUpIcon").click( function() {
        mLevelManager.getLevel().myChar.fireEvent("abilityLeveled", {
            abilityId: MainCharacter.SECOND_ABILITY
        });
    });
    $("#thirdAbilityLevelUpIcon").click( function() {
        mLevelManager.getLevel().myChar.fireEvent("abilityLeveled", {
            abilityId: MainCharacter.THIRD_ABILITY
        });
    });
    $("#fourthAbilityLevelUpIcon").click( function() {
        mLevelManager.getLevel().myChar.fireEvent("abilityLeveled", {
            abilityId: MainCharacter.FOURTH_ABILITY
        });
    });
    $("#firstAbilityCon").click( function() {
        if (mLevelManager.getGameState().getState() == mLevelManager.getGameState().getStateValues().SIMULATION) {
            mLevelManager.getLevel().myChar.setActiveAbility(MainCharacter.FIRST_ABILITY);
        }
    });
    $("#secondAbilityCon").click( function() {
        if (mLevelManager.getGameState().getState() == mLevelManager.getGameState().getStateValues().SIMULATION) {
            mLevelManager.getLevel().myChar.setActiveAbility(MainCharacter.SECOND_ABILITY);
        }
    });
    $("#thirdAbilityCon").click( function() {
        if (mLevelManager.getGameState().getState() == mLevelManager.getGameState().getStateValues().SIMULATION) {
            mLevelManager.getLevel().myChar.setActiveAbility(MainCharacter.THIRD_ABILITY);
        }
    });
    $("#fourthAbilityCon").click( function() {
        if (mLevelManager.getGameState().getState() == mLevelManager.getGameState().getStateValues().SIMULATION) {
            mLevelManager.getLevel().myChar.setActiveAbility(MainCharacter.FOURTH_ABILITY);
        }
    });
    $("#startButton").unbind('click');
    $("#startButton").click( function() {
        myself.fireEvent("pausedPressed", {});
    });

    document.getElementById("canvas").onmouseover = function(e) {
        mouseOverCanvas = true;
    };

    document.getElementById("canvas").onmouseout = function(e) {
        mouseOverCanvas = false;
    };

    /**
     * Event is triggeres if the mouse is clicked on the field. Sets
     * the target of the main character.
     */
    document.onmousedown = function(e) {
        if (mouseOverCanvas) {
            if (mLevelManager.getGameState().getState() == mLevelManager.getGameState().getStateValues().SIMULATION) {
                var mousepos = mouse.getMousePosition();

                mousepos.x = mousepos.x - $("#game_container").offset().left;
                mousepos.y = mousepos.y - $("#game_container").offset().top;

                if (mousepos.x && mousepos.y) {
                    var pickResult = mLevelManager.getGameScene().pick(mousepos.x, mousepos.y);

                    mLevelManager.processMouseInput([pickResult.coord[0], pickResult.coord[1], 0], pickResult.object);
                }
            }
        }
    };
};

extend(InputHandler, Events);
