var NPCCharacter = (function(){
    return function(mainScene, documentName, terrain){
        Character.call(this, mainScene, documentName, terrain);
        this.addState("STUNNED");
        var criticalStrike;
        var points = 100;
        var critChance = 10;
        var slowed = false;
        var stunTime = 60;
        var existanceTime = 0;
        var myself = this;

        this.isSlowed = function(){
            return slowed;
        };
        this.setSlowed = function(val){
            slowed = val;
        };
        /**
         * Sets the level of the NPCCharacter, all properties are set here
         * @param {number} lv
         */
        this.setLevel = function(lv){
            critChance += 3;
            level = lv;
            this.setMaxHealth(100 + (lv - 1) * 10);
            this.setDamage(this.getDamage() + 1.5 * (level - 1));
            this.setCritical(this.getDamage() * 1.5);

            this.setPoints(80 + 5 * (level - 1) * level);
        };
        /**
         * Returns the criticalStrike
         */
        this.getCritical = function(){
            return criticalStrike;
        };
        /**
         * Sets the criticalStrike damage
         * @param {number} crit
         */
        this.setCritical = function(crit){
            criticalStrike = crit;
        };
        /**
         * Returns the points the unit gives when it is killed.
         */
        this.getPoints = function(){
            return points;
        };
        /**
         * Sets the points.
         * @param {number} pn
         */
        this.setPoints = function(pn){
            points = pn;
        };
        /**
         * Checks if a critical strike has accoured.
         *
         */
        this.checkForCriticalStrike = function(){
            var rand_no = Math.ceil(100 * Math.random());
            if (rand_no <= critChance) {
                return true;
            }
            return false;
        };
        /**
         * Function is called when health is depleated. Removes
         *the NPCCharacter from the field.
         */
        this.die = function(){
            this.setState("DEATH");
        };

        /**
         * If the NPCCharacter is low on health, it will
         * stop chasing the main character.
         * TESTING!
         */
        this.runAway = function(){

            if (this.getHealth() < 10) {
                this.isRepulsedFrom(this.getTarget().getLoc());
            }
            else {
                this.isAttractedTo(this.getTarget().getLoc());
            }
        };
        /**
         * If the NPCCharacter is at half health or less, its velocity
         * will decrease.
         *
         */
        this.slowDown = function(){
            if ((this.getHealth() < this.getMaxHealth() / 2) && (this.isSlowed() == false)) {
                this.setVelocity(this.getVelocity() / 2);
                this.setSlowed(true);
                return;
            }

            if (this.isSlowed() == true && this.getHealth() > this.getMaxHealth() / 2) {
                this.setVelocity(this.getVelocity() * 2);
                this.setSlowed(false);
            }
        };
        this.updateStunned = function(){
            if (this.getState() == this.states.STUNNED) {
                stunTime--;
            }
            if (this.getState() == this.states.STUNNED && stunTime == 0) {
                stunTime = 60;
                this.setState("MOVE");
                return;
            }
        };

        /**
         * Checks the state of the character and sets the animation accordingly.
         */
        this.changeAnimation = function(){
            if (this.getState() == this.states.STAND_STILL) {
                this.getColladaObject().setStartFrame(90, 200, true);
                this.getColladaObject().setFrames(20);
                this.getColladaObject().setFrameRate(15);
            }
            if (this.getState() == this.states.MOVE) {
                this.getColladaObject().setStartFrame(16, 200, true);
                this.getColladaObject().setFrames(10);
                this.getColladaObject().setFrameRate(10);
            }
            if (this.getState() == this.states.ATTACK) {
                this.getColladaObject().setStartFrame(112, 200, false);
                this.getColladaObject().setFrames(12);
                this.getColladaObject().setFrameRate(15);
            }
            if (this.getState() == this.states.DEATH) {
                this.getColladaObject().setStartFrame(230, 200, false);
                this.getColladaObject().setFrames(25);
                this.getColladaObject().setFrameRate(25);
                var myDeath = setInterval( function() {
                    if (myself.getColladaObject().getFrameNumber() >= 247) {
                        clearInterval(myDeath);
                        myself.setZ(-50);
                    }
                }, 1);
            }
            if (this.getState() == this.states.STUNNED) {
                this.getColladaObject().setStartFrame(230, 200, false);
                this.getColladaObject().setFrames(15);
                this.getColladaObject().setFrameRate(10);
            }
        };


        /**
         * Updates the attack of the NPCCharacter.
         */
        this.updateAttack = function(){
            if (this.checkForCriticalStrike() == false)
                this.getTarget().getHit(this.getDamage());
            else
                this.getTarget().getHit(criticalStrike);
            this.setCurrentAttackCooldown(this.getAttackCooldownInterval());
            this.setState("ATTACK_COOLDOWN");
        };

        /**
         * Updates the state of the NPCCharacter.
         * @param {Array} obstacles
         */
        this.updateLogic = function(obstacles){
            if (this.getState() == this.states.DEATH)
                return;
            this.updateStunned();
            this.slowDown();
            //Animations fix
            ////////////////////////////
            if (existanceTime < 8)
                existanceTime++;
            if (existanceTime == 8) {
                this.changeAnimation();
                existanceTime = 9;
            }
            ///////////////////////////
            if (this.getState() != this.states.STUNNED) {
                this.updateState();
            }
            else {
                return;
            }

            if ((this.getState() != this.states.STAND_STILL) && (this.getState() != this.states.DEATH)) {
                this.updateHpRegen();
                if (this.getState() == this.states.MOVE) {
                    this.updateMovement(obstacles);
                }
                if (this.getState() == this.states.ATTACK) {
                    this.updateAttack();
                    this.Lookat(this.getTarget());
                }
                if (this.getState() == this.states.ATTACK_COOLDOWN) {
                    this.Lookat(this.getTarget());
                }
            }
        };
    };
})();

extend(NPCCharacter, Character);
