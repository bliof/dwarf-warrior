var GameState = function() {
    /*
     * These are the states in which the game can be.
     */
    var stateValues = {
        LOADING : 1,
        MENU : 2,
        SIMULATION : 3,
        PAUSEDSIMULATION : 4,
        END : 5
    };
    var state;
    /**
     * Returns the state of the game.
     */
    this.getState = function() {
        return this.state;
    };
    /**
     * Returns the states in which the game can be.
     */
    this.getStateValues = function() {
        return stateValues;
    };

    /**
     * Sets the state.
     *
     * @param {Object}
     *            newState - the new state of the game
     */
    this.setState = function(newState) {
        if (stateValues[newState] && (stateValues[newState] != state)) {
            this.state = stateValues[newState];
            return true;
        } else {
            return false;
        }
    };

    this.setState("MENU");
};
