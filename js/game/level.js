var Level = (function(){
    return function(){
        this.myChar;
        this.enemies = [];
        this.terrain;
        this.nonMovingObjects = [];
        this.doc;

        this.clearLevel = function(gameScene){
            for (var i in this.enemies) {
                gameScene.removeChild(this.enemies[i]);
            }
            for (var i in this.nonMovingObjects) {
                gameScene.removeChild(this.nonMovingObjects[i].getColladaObject());
            }
            gameScene.removeChild(this.terrain.getColladaObject());
            gameScene.removeChild(this.myChar.getColladaObject());
        };
    };
})();
