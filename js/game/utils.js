/**
 * Resolving the end of the game. Creates a div in which
 * shows the score earned by the player.
 * One argument needed which must have property named "score".
 *
 * @param e
 */
function buildEndGameScreen(e) {
    var ni = document.getElementById('container');

    var newdiv = document.createElement('div');
    var divIdName = 'scoreContainer';
    newdiv.setAttribute('id', divIdName);
    newdiv.innerHTML = 'GAME OVER. <br> Score is: ' + e.score;
    newdiv.style.fontSize = '30';
    newdiv.style.textAlign = 'center';
    newdiv.style.paddingTop = '20';
    ni.appendChild(newdiv);
    document.getElementById("game_container").style.visibility = 'hidden';
}

/**
 * Extends a type. Used for inheritance. The subClass extends the superClass.
 *
 * @param {class}
 *            subClass
 * @param {class}
 *            superClass
 */
function extend(subClass, superClass) {
    var F = function() {};
    F.prototype = superClass.prototype;
    subClass.prototype = new F();
    subClass.prototype.constructor = subClass;
    subClass.superclass = superClass.prototype;
    if (superClass.prototype.constructor == Object.prototype.constructor) {
        superClass.prototype.constructor = superClass;
    }
}

/**
 * Centers all elements on the screen
 */
function setElementsPositions(){
    var width = $("#container").width() / 2;
    $(".menu").css({
        left: width - 30,
        top: 250
    });
    $("#gameTitle").css({
        left: width -150,
        top: 50
    });
    $("#game_container").css({
        left: width - $("#game_container").width() / 2
    });

    var abilitiesCSSLeft = $("#game_container").width() / 2 - $("#abilities").width() / 2;
    $("#abilities").css({
        left: abilitiesCSSLeft,
        top: $("#game_container").height() - $("#abilities").height() - 6
    });

    $(".iconContainer").height($(".iconContainer").width());
    $(".ability_icon").width($(".iconContainer").width());

    var tmpLeft = abilitiesCSSLeft;
    tmpLeft += $("#secondAbilityCon").position().left - $("#firstAbilityCon").width();
    tmpLeft += $("#firstAbilityCon").width() / 2 - $("#firstAbilityLevelUpIcon").width() / 2;

    $("#firstAbilityLevelUpIcon").css({
        left: tmpLeft
    });
    tmpLeft = abilitiesCSSLeft;
    tmpLeft += $("#thirdAbilityCon").position().left - $("#secondAbilityCon").width();
    tmpLeft += $("#secondAbilityCon").width() / 2 - $("#secondAbilityLevelUpIcon").width() / 2;
    $("#secondAbilityLevelUpIcon").css({
        left: tmpLeft
    });

    tmpLeft = abilitiesCSSLeft;
    tmpLeft += $("#fourthAbilityCon").position().left - $("#thirdAbilityCon").width();
    tmpLeft += $("#thirdAbilityCon").width() / 2 - $("#thirdAbilityLevelUpIcon").width() / 2;
    $("#thirdAbilityLevelUpIcon").css({
        left: tmpLeft
    });

    tmpLeft = abilitiesCSSLeft;
    tmpLeft += $("#fourthAbilityCon").position().left + $("#secondAbilityCon").position().left - $("#thirdAbilityCon").width();
    tmpLeft += $("#fourthAbilityCon").width() / 2 - $("#fourthAbilityLevelUpIcon").width() / 2;
    $("#fourthAbilityLevelUpIcon").css({
        left: tmpLeft
    });

    $(".plus").css({
        bottom: $("#abilities").height() + 10
    });
}

/**
 * sets the size the game_container and the canvas element
 *
 * @param width
 * @param height
 */
function setGameWindowSize(width, height){
    $("#canvas")[0].width = width;
    $("#canvas")[0].height = height;
    $("#game_container").width(width);
    $("#game_container").height(height);
}
