var LevelBuilder = function(){
    LevelBuilder.superclass.constructor.call(this);
    myself = this;

    this.createLevel = function(gameScene, gameLevel, doc){
        var level = new Level();
        level.doc = doc;
        addNonMovingObjects(level);
        createTerrain(gameScene, gameLevel, level);

        level.terrain.getColladaObject().addEventListener("loaded", function(){
            level.terrain.setHeightMap("./colladaFiles/terr/heightmap.png");
            var objectsInCollada = level.terrain.getColladaObject().getObjects();
             for (var j in objectsInCollada) {
                 if(objectsInCollada[j].getId()=="Mesh_042-mesh-mesh"
                     || objectsInCollada[j].getId()=="Mesh_027-mesh-mesh"
          //           || objectsInCollada[j].getId()=="Mesh_026-mesh-mesh"
                     || objectsInCollada[j].getId()=="Mesh_015-mesh-mesh"
                     || objectsInCollada[j].getId()=="Mesh_013-mesh-mesh"
                     || objectsInCollada[j].getId()=="Mesh_011-mesh-mesh"
                     || objectsInCollada[j].getId()=="Mesh_009-mesh-mesh"    )
                objectsInCollada[j].setZtransparent(true);
            }

            createMainCharacter(gameScene, gameLevel, level);

            level.myChar.getColladaObject().addEventListener("loaded", function(){
                initializeNPCCharacters(gameScene, gameLevel, level);

                myself.resetEnemiesStats(gameScene, gameLevel, level);

                var j = 0;
                for (i = 0; i < 4; i++) {
                    level.enemies[i].getColladaObject().addEventListener("loaded", function(){
                        j++;
                        $("#loading").html("Loaded meshes...");
                        if (j == 4) {
                            myself.fireEvent("sceneLoaded", {});
                        }
                    });
                }
            });
        });
        return level;
    };
    function createTerrain(gameScene, gameLevel, level){
        level.terrain = new Terrain(gameScene, "./colladaFiles/terr.dae");
        level.terrain.setScale(15);
        level.terrain.setZ(0);
    }

    function createMainCharacter (gameScene, gameLevel, level){
        level.myChar = new MainCharacter(gameScene, "./colladaFiles/mainChar.DAE", level.terrain, level.doc);
        level.myChar.setScale(0.5);
        level.myChar.setZ(20);
    };

    function initializeNPCCharacters(gameScene, gameLevel, level){
        var obst1 = new NPCCharacter(gameScene, "./colladaFiles/enemies.DAE", level.terrain);
        obst1.setScale(0.5);
        level.enemies.push(obst1);
        var obst2 = new NPCCharacter(gameScene, "./colladaFiles/enemies.DAE", level.terrain);
        obst2.setScale(0.5);
        level.enemies.push(obst2);
        var obst3 = new NPCCharacter(gameScene, "./colladaFiles/enemies.DAE", level.terrain);
        obst3.setScale(0.5);
        level.enemies.push(obst3);
        var obst4 = new NPCCharacter(gameScene, "./colladaFiles/enemies.DAE", level.terrain);
        obst4.setScale(0.5);
        level.enemies.push(obst4);
    };

    this.resetEnemiesStats = function(gameScene, gameLevel, level){
        level.enemies[0].setHealth(level.enemies[0].getMaxHealth());
        level.enemies[0].setDamage(gameLevel);
        level.enemies[0].setVelocity(10);
        level.enemies[0].setLoc([70, -70, 0]);
        level.enemies[0].setTarget(level.myChar);
        level.enemies[0].setLevel(gameLevel);
        level.enemies[0].setState("MOVE");


        level.enemies[1].setHealth(level.enemies[1].getMaxHealth());
        level.enemies[1].setDamage(gameLevel);
        level.enemies[1].setVelocity(10);
        level.enemies[1].setLoc([-70, -70, 0]);
        level.enemies[1].setTarget(level.myChar);
        level.enemies[1].setLevel(gameLevel);
        level.enemies[1].setState("MOVE");


        level.enemies[2].setHealth(level.enemies[2].getMaxHealth());
        level.enemies[2].setDamage(gameLevel);
        level.enemies[2].setVelocity(10);
        level.enemies[2].setLoc([70, 70, 0]);
        level.enemies[2].setTarget(level.myChar);
        level.enemies[2].setLevel(gameLevel);
        level.enemies[2].setState("MOVE");


        level.enemies[3].setHealth(level.enemies[3].getMaxHealth());
        level.enemies[3].setDamage(gameLevel);
        level.enemies[3].setVelocity(10);
        level.enemies[3].setLoc([-70, 70, 0]);
        level.enemies[3].setTarget(level.myChar);
        level.enemies[3].setLevel(gameLevel);
        level.enemies[3].setState("MOVE");
    };

    function addNonMovingObjects(level) {
        level.nonMovingObjects[0] = new Obstacle(121.97, -121.4, 35.3, 33000);
        level.nonMovingObjects[1] = new Obstacle(-127.18, -66, 11.06, 100000);
        level.nonMovingObjects[2] = new Obstacle(54.21, 145.01, 29.81, 100000);
        level.nonMovingObjects[3] = new Obstacle(120.07, 70.44, 8.78, 15000);
    }
};

extend(LevelBuilder, Events);
